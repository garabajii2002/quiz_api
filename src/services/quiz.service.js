const admin = require('firebase-admin');

const db = admin.firestore();

class QuizService {
  static async checkQuestion({questionId, response}) {
    try {
      const ref = db.doc(`questions/${questionId}`);
      return ref.get().then(doc => {
        if (doc.exists) {
          const question = doc.data();

          const isResponseValid = question.responseList.find(res => res === response);

          const isCorrect = isResponseValid && question.correctResponse === response;

          return {
            status: 200,
            message: 'OK',
            data: {
              isCorrect
            }
          }
        } else {
          return {
            status: 404,
            message: 'Not Found'
          }
        }
      });
    } catch (e) {
      return {
        status: 400,
        message: 'Error happened',
        error: e
      }
    }
  }
}

module.exports = QuizService;
