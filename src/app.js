const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const cors = require('cors');
const admin = require('firebase-admin');
const serviceAccount = require("./config/keys/quiz-api");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: 'https://quiz-api-34a7b.firebaseio.com'
});

const app = express();

app.use(cors());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// init firebase
const firebase = require('firebase');

// firebase.initializeApp(config.get('firebaseConfig'));

// init routers
const quizRouter = require('./routes/quiz.router');

app.use('/api/questions', quizRouter);


app.listen(3002, () => {
  console.log('API SERVER LISTENING ON 3002')
})
